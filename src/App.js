import React from "react";
import "./App.css";
import DashboardRoutes from "./routes/dashboardRoutes/DashboardRoutes";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from "./components/login/Login";
import SignUp from "./components/signup/SignUp";


function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={SignUp} />
          <DashboardRoutes />
        </Switch>
      </Router>
    </>
  );
}

export default App;
